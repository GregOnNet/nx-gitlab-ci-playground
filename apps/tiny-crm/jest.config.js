module.exports = {
  name: 'tiny-crm',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/tiny-crm',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
