import { Component } from '@angular/core';

@Component({
  selector: 'nx-gitlab-ci-playground-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tiny-crm';
}
