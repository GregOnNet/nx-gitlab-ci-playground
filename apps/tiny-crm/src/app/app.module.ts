import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MobileNavigationModule } from '@mobile/navigation';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, MobileNavigationModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
