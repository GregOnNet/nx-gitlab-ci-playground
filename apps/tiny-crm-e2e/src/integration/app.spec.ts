import { getGreeting } from '../support/app.po';

describe('tiny-crm', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to tiny-crm!');
  });
});
