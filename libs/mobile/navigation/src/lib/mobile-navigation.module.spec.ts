import { async, TestBed } from '@angular/core/testing';
import { MobileNavigationModule } from './mobile-navigation.module';

describe('MobileNavigationModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MobileNavigationModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(MobileNavigationModule).toBeDefined();
  });
});
