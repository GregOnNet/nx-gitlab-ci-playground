module.exports = {
  name: 'mobile-navigation',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/mobile/navigation',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
